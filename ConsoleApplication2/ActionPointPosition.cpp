#include "ActionPointPosition.h"



ActionPointPosition::ActionPointPosition(cv::Point2d ancor, int informationId)
{
	this->ancor = ancor;
	this->informationId = informationId;
}


ActionPointPosition::~ActionPointPosition()
{
}

cv::Point2d ActionPointPosition::getAncor() {
	return ancor;
}

void ActionPointPosition::setAncor(cv::Point2d newAncor) {
	ancor = newAncor;
}