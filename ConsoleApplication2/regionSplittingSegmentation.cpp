//
//
//
//
//#include <opencv2/highgui.hpp>
//#include <opencv2/core.hpp>
//#include <opencv2/imgproc.hpp>
//#include <iostream>
//
//using namespace cv;
//
//Mat img;
//Size size;
//
//struct region {
//    // tree data structure
//	std::vector<region> childs;
//    bool validity; // TODO: have a method for clear the data structure and remove regions with false validity
//
//    // tree for split&merge procedure
//    Rect roi;
//    Mat m;
//    Scalar label;
//    Mat mask; // for debug. don't use in real cases because it is computationally too heavy.
//};
//
////----------------------------------------------------------------------------------------------------------------------- merging
//bool mergeTwoRegion(region& parent, const Mat& src, region& r1, region& r2,  bool (*predicate)(const Mat&)) {
//    if(r1.childs.size()==0 && r2.childs.size()==0) {
//
//        Rect roi1 = r1.roi;
//        Rect roi2 = r2.roi;
//        Rect roi12 = roi1 | roi2;
//        if(predicate( src(roi12) )) {
//            r1.roi = roi12;
//
//            // recompute mask
//            r1.mask = Mat::zeros(size, CV_8U);
//            rectangle(r1.mask, r1.roi, 1, CV_FILLED);
//
//            r2.validity = false;
//            return true;
//        }
//    }
//    return false;
//}
//
//void merge(const Mat& src, region& r, bool (*predicate)(const Mat&)) {
//    // check for adjiacent regions. if predicate is true, then  merge.
//    // the problem is to check for adjiacent regions.. one way can be:
//    // check merging for rows. if neither rows can be merged.. check for cols.
//
//    bool row1=false, row2=false, col1=false, col2=false;
//
//    if(r.childs.size()<1) return;
//
//    // try with the row
//    row1 = mergeTwoRegion(r, src, r.childs[0], r.childs[1], predicate);
//    row2 = mergeTwoRegion(r, src, r.childs[2], r.childs[3], predicate);
//
//    if( !(row1 | row2) ) {
//        // try with column
//        col1 = mergeTwoRegion(r, src, r.childs[0], r.childs[2], predicate);
//        col2 = mergeTwoRegion(r, src, r.childs[1], r.childs[3], predicate);
//    } 
//
//    for(int i=0; i<r.childs.size(); i++) {
//        if(r.childs[i].childs.size()>0) 
//            merge(src, r.childs[i], predicate);
//    }
//}
//
////----------------------------------------------------------------------------------------------------------------------- quadtree splitting
//region split(const Mat& src, Rect roi, bool (*predicate)(const Mat&)) {
//	std::vector<region> childs;
//    region r;
//
//    r.roi = roi;
//    r.m = src;
//    r.mask = Mat::zeros(size, CV_8U);
//    rectangle(r.mask, r.roi, 1, CV_FILLED);
//    r.validity = true;
//
//    bool b = predicate(src);
//    if(b) {
//        Scalar mean, s;
//        meanStdDev(src, mean, s);
//        r.label = mean;
//    } else {
//        int w = src.cols/2;
//        int h = src.rows/2;
//        region r1 = split(src(Rect(0,0, w,h)), Rect(roi.x, roi.y, w,h), predicate);
//        region r2 = split(src(Rect(w,0, w,h)), Rect(roi.x+w, roi.y, w,h), predicate);
//        region r3 = split(src(Rect(0,h, w,h)), Rect(roi.x, roi.y+h, w,h), predicate);
//        region r4 = split(src(Rect(w,h, w,h)), Rect(roi.x+w, roi.y+h, w,h), predicate);
//        r.childs.push_back( r1 );
//        r.childs.push_back( r2 );
//        r.childs.push_back( r3 );
//        r.childs.push_back( r4 );
//    }
//    //merge(img, r, predicate);
//    return r;
//}
//
////----------------------------------------------------------------------------------------------------------------------- tree traversing utility
//void print_region(region r) {
//    if(r.validity==true && r.childs.size()==0) {
//		std::cout << r.mask << " at " << r.roi.x << "-" << r.roi.y << std::endl;
//		std::cout << r.childs.size() << std::endl;
//		std::cout << "---" << std::endl;
//    }
//    for(int i=0; i<r.childs.size(); i++) {
//        print_region(r.childs[i]);
//    }
//}
//
//void draw_rect(Mat& imgRect, region r) {
//    if(r.validity==true && r.childs.size()==0) 
//        rectangle(imgRect, r.roi, 50, .1);
//    for(int i=0; i<r.childs.size(); i++) {
//        draw_rect(imgRect, r.childs[i]);
//    }
//}
//
//void draw_region(Mat& img, region r) {
//    if(r.validity==true && r.childs.size()==0) 
//        rectangle(img, r.roi, r.label, CV_FILLED);
//    for(int i=0; i<r.childs.size(); i++) {
//        draw_region(img, r.childs[i]);
//    }
//}
//
////----------------------------------------------------------------------------------------------------------------------- split&merge test predicates
//bool predicateStdZero(const Mat& src) {
//    Scalar stddev, mean;
//    meanStdDev(src, mean, stddev);
//    return stddev[0]==0;
//}
//bool predicateStd5(const Mat& src) {
//    Scalar stddev, mean;
//    meanStdDev(src, mean, stddev);
//    return (stddev[0]<=5.8) || (src.rows*src.cols<=50);
//}
//
////----------------------------------------------------------------------------------------------------------------------- main
//int main( int /*argc*/, char** /*argv*/ )
//{
//   /* img = (Mat_<uchar>(4,4) << 0,0,1,1,
//                               1,1,1,1, 
//                               3,3,3,3,
//                               3,4,4,3);
//
//	std::cout << img << std::endl;
//    size = img.size();
//
//    region r;
//    r = split(img, Rect(0,0,img.cols,img.rows), &predicateStdZero);
//    merge(img, r, &predicateStdZero);
//	std::cout << "------- print" << std::endl;
//    print_region(r);
//
//	std::cout << "-----------------------" << std::endl;
//*/
//    img = imread("image008.jpg", 0);
//	Mat out = img.clone();
//	GaussianBlur(img, out, Size(9, 9), 6, 6);
//	img = out;
//
//    // round (down) to the nearest power of 2 .. quadtree dimension is a pow of 2.
//    int exponent = log(min(img.cols, img.rows)) / log (2);
//    int s = pow(2.0, (double)exponent);
//    Rect square = Rect(0,0, s,s);
//    img = img(square).clone();
//
//    namedWindow("original", CV_WINDOW_AUTOSIZE);
//    imshow( "original", img );
//
//	std::cout << "now try to split.." << std::endl;
//	region r = split(img, Rect(0,0,img.cols,img.rows), predicateStd5);
//
//	std::cout << "splitted" << std::endl;
//    Mat imgRect = img.clone();
//    draw_rect(imgRect, r);
//    namedWindow("split", CV_WINDOW_AUTOSIZE);
//    imshow( "split", imgRect );
//    imwrite("split.jpg", imgRect);
//
//    merge(img, r, &predicateStd5);
//    Mat imgMerge = img.clone();
//    draw_rect(imgMerge, r);
//    namedWindow("merge", CV_WINDOW_AUTOSIZE);
//    imshow( "merge", imgMerge );
//    imwrite( "merge.jpg", imgMerge );
//
//    Mat imgSegmented = img.clone();
//    draw_region(imgSegmented, r);
//    namedWindow("segmented", CV_WINDOW_AUTOSIZE);
//    imshow( "segmented", imgSegmented );
//    imwrite( "segmented.jpg", imgSegmented );
//
//	threshold(imgSegmented, imgSegmented, 128, 255, THRESH_BINARY);
//	namedWindow("threshold", CV_WINDOW_AUTOSIZE);
//	imshow("threshold", imgSegmented);
//	imwrite("threshold.jpg", imgSegmented);
//
//	std::vector<std::vector<Point> > contours;
//	std::vector<Vec4i> hierarchy;
//	findContours(imgSegmented, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
//	int index = 0;
//	double maxsize = 0;
//	for( int i = 0; i< contours.size(); i++ ) {
//		double size = contourArea(contours[i]);
//		if(size > maxsize) {
//			index = i;
//			maxsize = size;
//		}
//	}
//		
//	Scalar color = Scalar(80, 80, 80);
//	drawContours(img, contours, index, color, 2, 8, hierarchy, 0, Point() );
//	namedWindow("findContours", CV_WINDOW_AUTOSIZE);
//	imshow("findContours", img);
//	imwrite("findContours.jpg", img);
//
//
//    while( true )
//    {
//        char c = (char)waitKey(10);
//        if( c == 27 ) { break; }
//    }
//
//    return 0;
//}
//
////// im2dots.cpp
////// OpenCV code to achieve similar photo effect as shown in 
////// http://photoshopessentials.com/photo-effects/color-dots/
////
////#include <opencv2/imgproc/imgproc.hpp>
////#include <opencv2/highgui/highgui.hpp>
////
////int main()
////{
////    cv::Mat src = cv::imread("image008.jpg");
////    if (!src.data)
////        return -1;
////
////    cv::Mat dst = cv::Mat::zeros(src.size(), CV_8UC3);
////    cv::Mat cir = cv::Mat::zeros(src.size(), CV_8UC1);
////    int bsize = 10;
////
////    for (int i = 0; i < src.rows; i += bsize)
////    {
////        for (int j = 0; j < src.cols; j += bsize)
////        {
////            cv::Rect rect = cv::Rect(j, i, bsize, bsize) & 
////                            cv::Rect(0, 0, src.cols, src.rows);
////
////            cv::Mat sub_dst(dst, rect);
////            sub_dst.setTo(cv::mean(src(rect)));
////
////
////			cv::rectangle 	( cir,
////				 Rect,
////				const Scalar &  	color,
////				int  	thickness = 1,
////				int  	lineType = LINE_8,
////				int  	shift = 0 
////			) 	
////
////            cv::rectangle(
////                cir, 
////                cv::Point(j+bsize/2, i+bsize/2), 
////                bsize/2-1, 
////                CV_RGB(255,255,255), -1, CV_AA
////            );
////        }
////    }
////
////    cv::Mat cir_32f;
////    cir.convertTo(cir_32f, CV_32F);
////    cv::normalize(cir_32f, cir_32f, 0, 1, cv::NORM_MINMAX);
////
////    cv::Mat dst_32f;
////    dst.convertTo(dst_32f, CV_32F);
////
////    std::vector<cv::Mat> channels;
////    cv::split(dst_32f, channels);
////    for (int i = 0; i < channels.size(); ++i)
////        channels[i] = channels[i].mul(cir_32f);
////
////    cv::merge(channels, dst_32f);
////    dst_32f.convertTo(dst, CV_8U);
////
////    cv::imshow("dst", dst);
////    cv::waitKey();
////    return 0;
////}
////
//////#include <stdio.h>
//////#include <iostream>
//////#include <stdio.h>
//////#include <iostream>
//////#include "opencv2/core.hpp"
//////#include "opencv2/features2d.hpp"
//////#include "opencv2/imgcodecs.hpp"
//////#include "opencv2/highgui.hpp"
//////#include "opencv2/xfeatures2d.hpp"
//////
//////using namespace std;
//////using namespace cv;
//////using namespace cv::xfeatures2d;
//////#include <opencv2/opencv.hpp>
//////
//////RNG rng(12345);
//////
//////int main(int argc, char *argv[])
//////{
//////    Mat image = imread("image008.jpg");
//////	GaussianBlur(image, image, Size(5,5), 0);
//////	Mat mserOutMask = image.clone();
//////	Ptr<MSER> mserExtractor  = MSER::create();
//////
//////    vector<vector<cv::Point>> mserContours;
//////    vector<KeyPoint> mserKeypoint;
//////    vector<cv::Rect> mserBbox;
//////    mserExtractor->detectRegions(image, mserContours,  mserBbox);
//////
//////    
//////
//////	 Mat image2 = imread("image009.jpg");
//////	 GaussianBlur(image2, image2, Size(5,5), 0);
//////	Mat mserOutMask2 = image2.clone();
//////	Ptr<MSER> mserExtractor2  = MSER::create();
//////
//////    vector<vector<cv::Point>> mserContours2;
//////    vector<KeyPoint> mserKeypoint2;
//////    vector<cv::Rect> mserBbox2;
//////    mserExtractor2->detectRegions(image2, mserContours2,  mserBbox2);
//////
//////   
//////   double minContourSize = 10.0; 
//////
//////	for (int i = 0; i < mserContours.size(); i++) {
//////		if (contourArea(mserContours[i]) > minContourSize) {
//////			for (int j = 0; j < mserContours2.size(); j++) {
//////				if (contourArea(mserContours2[j]) > minContourSize) {
//////					double d = matchShapes(mserContours[i], mserContours2[j], 1, 0);
//////					if (d < 0.05) {
//////						Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
//////						drawContours(mserOutMask2, mserContours2, j , color);
//////						drawContours(mserOutMask, mserContours, i , color);
//////					}
//////				}
//////			}
//////		}
//////	}
//////
//////	imshow("mser", mserOutMask);
//////	 imshow("mser2", mserOutMask2);
//////
//////    waitKey(0);
//////    return 0;
//////}