#pragma once
class MatchingPointDescription
{
private:
	float angle;
	float distance;

public:
	MatchingPointDescription(float angle, float distance);
	~MatchingPointDescription();
	float getAngle();
	float getDistance();
};

