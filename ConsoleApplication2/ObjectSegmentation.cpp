#include <stdio.h>
#include <iostream>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include "opencv2/core.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/features2d.hpp"
#include <opencv2/opencv.hpp>


using namespace std;
using namespace cv;

void brandenburderTor(string imageNumber);
void bundestag();
void dom();
void fernsehturm(int number);

void RGBtoGray();
void GraytoRGB();
void RGBtoHSV();
void HSVtoRGB();
void canny(double t1, double t2);
void laplac();
void gaus(int size);
void thres(double t1, double t2, int type);
void thresBetween(double max, double min);
vector<vector<Point> > contour(bool max = false);
void closing(int amount = 1);
void opening(int amount = 1);
vector<Vec2f> hough(int minVotes);
vector<Vec4i> houghP(int minVotes);
void hsvFilter(int a, int b, int c, int d, int e, int f);
void detectBlobs();
void histEque();
Point2f findCenter(bool all);
void crop();
void shap();
void leftRightFloodFill();
void matchMaxShape (vector<Point> shape);

RNG rng(12345);
Mat image;
String imagePath = "toor\\07.jpg";
stringstream commands;

int main(int argc, char *argv[]) {

	std::string names[] = { "01",
		/*"02",
		"03",
		"04",
		"05",
		"06",
		"07",
		"08",
		"09",
		"10",
		"11",
		"12",
		"13",
		"14",
		"15",
		"16",
		"17",
		"18",
		"19",
		"20",
		"21",
		"22",
		"23",
		"24",
		"25",
		"26",
		"27",
		"28",*/
		"29",
		"30",
		/*"31",
		"32",
		"33",
		"34",
		"35",
		"36",
		"37",
		"38",
		"39",
		"40",
		"41",
		"42",
		"43",
		"44",
		"45",
		"46",*/
		"47" };

	for (std::string imageNumber : names)
	{
		brandenburderTor(imageNumber);

		/*image = imread("_tor/"+imageNumber+".jpg");
		RGBtoHSV();
		hsvFilter(0, 40, 40, 60, 255, 255);
		closing(4);
		vector<vector<Point>> ca = contour(true);


		Mat test = imread("_tor/" + imageNumber + ".jpg", IMREAD_GRAYSCALE);
		vector<vector<Point> > contours;
		vector<Vec4i> hierarchy;

		findContours(test, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));


		int index = 0;
		double maxsize = 0;
		for (int i = 0; i < contours.size(); i++) {
			double size = contourArea(contours[i]);
			if (size > maxsize) {
				index = i;
				maxsize = size;
			}
		}

		vector<vector<Point> > cb;
		cb.push_back(contours[index]);

		double x = matchShapes(ca[0], cb[0], CV_CONTOURS_MATCH_I2, 0);

		std::ofstream content;
		content.open("_tor/" + imageNumber + ".xml");
		content << commands.str();
		content.close();*/
	}
	waitKey(0);
}

void fernsehturm(int number) {
	image = imread(imagePath);
		
	commands << "<object>";
	crop();
	gaus(3);
	shap();
	leftRightFloodFill();
	RGBtoGray();
	thres(254, 255, 1);
	closing(2);
	vector<vector<Point>> mc = contour(true);

	double length = arcLength(mc[0], true);
	vector<Point> c;
	approxPolyDP(mc[0], c, 25, true);
	vector<vector<Point>> cc;
	cc.push_back(c);


	drawContours( image, cc, 0, Scalar(255,0,0), -1, 8); //, hierarchy, 0, Point() );

	resize(image, image, Size(800, 800));
	imshow(imagePath, image);

					
	commands << "</object>";

	ofstream myfile;
	myfile.open ("_turm/" + to_string(number) + ".xml");
	myfile << commands.str();
	myfile.close();

	commands.str(string());
	commands.clear();
}

void brandenburderTor(string imageNumber) {
	Ptr<ORB> detector = ORB::create();

	image = imread("_tor/" + imageNumber + ".jpg");

	commands << "<object>";
	RGBtoHSV();
	hsvFilter(0,40,40,60,255,255);
	closing(4);

	vector<vector<Point>> ca = contour(true);
	matchMaxShape(ca[0]);
		
	resize(image, image, Size(800, 800));
	imshow("_tor/" + imageNumber + ".jpg", image);

	commands << "</object>";

	ofstream myfile;
	myfile.open ("_tor/" + imageNumber + ".xml");
	myfile << commands.str();
	myfile.close();

	commands.str(string());
	commands.clear();
}

void bundestag() {
	Ptr<ORB> detector = ORB::create();

	image = imread(imagePath);
				
	RGBtoHSV();
	histEque();
	hsvFilter(0,40,40,60,255,255);

	Point2f p = findCenter(false);

	Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
    circle( image, p, 10, color, -1, 8, 0 );

	resize(image, image, Size(800, 800));
	imshow(imagePath, image);
}

void dom() {
	Ptr<ORB> detector = ORB::create();

	image = imread(imagePath);
				
	RGBtoHSV();
	hsvFilter(70,40,40,110,255,255);

	Point2f p = findCenter(false);
	Point2f main = p;

	Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
    circle( image, p, 10, color, -1, 8, 0 );

	resize(image, image, Size(800, 800));
	imshow(imagePath, image);

	image = imread(imagePath);
				
	RGBtoHSV();
	hsvFilter(0,90,90,40,255,255);

	p = findCenter(true);

	vector<Point2f> overMain;
	if (p.y < main.y) {
			overMain.push_back(p);
	}

	color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
	circle( image, overMain[0], 50, color, -1, 8, 0 );
		
	resize(image, image, Size(800, 800));
	imshow(imagePath, image);
}


void RGBtoGray() {
	Mat gray;
	cvtColor(image, gray, CV_BGR2GRAY);
	image = gray;
	commands << "<command name=\"RGBtoGray\" />" << endl;
}

void GraytoRGB() {
	Mat rgb;
	cvtColor(image, rgb, CV_GRAY2BGR);
	image = rgb;
	commands << "<command name=\"GraytoRGB\" />" << endl;
}

void RGBtoHSV() {
	Mat hsv;
	cvtColor(image, hsv, CV_BGR2HSV);
	image = hsv;
	commands << "<command name=\"RGBtoHSV\" />" << endl;
}

void HSVtoRGB() {
	Mat rgb;
	cvtColor(image, rgb, CV_HSV2BGR);
	image = rgb;
}

void canny(double t1, double t2) {
	Mat edges;
	Canny(image, edges, t1, t2);
	image = edges;
	commands << "<command name=\"canny\" >" << endl;
	commands << "<param value=\"" << t1 << "\" />" << endl;
	commands << "<param value=\"" << t2 << "\" />" << endl;
	commands << "</command>" << endl;
}

void histEque() {
	vector<Mat> channels;
    split(image,channels);

    equalizeHist(channels[0], channels[0]);

    Mat result;
    merge(channels,image);
	commands << "<command name=\"histEque\" />" << endl;
}


void laplac() {
	Mat dst;
	Laplacian( image, dst, CV_16S);
	convertScaleAbs( dst, image );
	commands << "<command name=\"laplac\" />" << endl;
}

void gaus(int size) {
	GaussianBlur(image, image, Size(size,size), 0, 0, BORDER_DEFAULT );
	commands << "<command name=\"gaus\" >" << endl;
	commands << "<param value=\"" << size << "\" />" << endl;
	commands << "</command>" << endl;
}

void thres(double t1, double t2, int type) {
	Mat dst;
	threshold(image, dst, t1, t2, type);
	image = dst;
	commands << "<command name=\"thres\" >" << endl;
	commands << "<param value=\"" << t1 << "\" />" << endl;
	commands << "<param value=\"" << t2 << "\" />" << endl;
	commands << "<param value=\"" << type << "\" />" << endl;
	commands << "</command>" << endl;
}

void thresBetween(double max, double min) {
	thres(max, 255, 4);
	thres(min, 255, 0);
	commands << "<command name=\"thresBetween\" >" << endl;
	commands << "<param value=\"" << max << "\" />" << endl;
	commands << "<param value=\"" << min << "\" />" << endl;
	commands << "</command>" << endl;
}

vector<vector<Point> > contour(bool max) {
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	findContours(image, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

	Mat drawing = Mat::zeros( image.size(), CV_8UC3 );
	
	if(max && contours.size() > 0) {
		int index = 0;
		double maxsize = 0;
		for( int i = 0; i< contours.size(); i++ ) {
			double size = contourArea(contours[i]);
			if(size > maxsize) {
				index = i;
				maxsize = size;
			}
		}
	
		Scalar color = Scalar(80, 80, 80);
		drawContours( drawing, contours, index, color, -1, 8, hierarchy, 0, Point() );

		vector<vector<Point> > c;
		c.push_back(contours[index]);
		contours = c;

	} else {
		for( int i = 0; i< contours.size(); i++ )
		{
		   Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
		   drawContours( drawing, contours, i, color, -1, 8, hierarchy, 0, Point() );
		}

	}
	image = drawing;

	commands << "<command name=\"contour\" >" << endl;
	commands << "<param value=\"" << max << "\" />" << endl;
	commands << "</command>" << endl;

	return contours;
}

void closing(int amount) {
	Mat element = getStructuringElement(MORPH_RECT, Size(3, 3));
	Mat dst; 
	for (int i = 0; i < amount; i++) {
		dilate( image, dst, element );
		image = dst;
	}
	for (int i = 0; i < amount; i++) {
		erode( dst, image, element );
		dst= image;
	}
	image = dst;
	commands << "<command name=\"closing\" >" << endl;
	commands << "<param value=\"" << amount << "\" />" << endl;
	commands << "</command>" << endl;
}

void opening(int amount) {
	Mat element = getStructuringElement(MORPH_RECT, Size(3, 3));
	Mat dst; 
	for (int i = 0; i < amount; i++) {
		erode( image, dst, element );
		image = dst;
	}

	for (int i = 0; i < amount; i++) {
		dilate( dst, image, element );
		dst= image;
	}
	image = dst;
	commands << "<command name=\"opening\" >" << endl;
	commands << "<param value=\"" << amount << "\" />" << endl;
	commands << "</command>" << endl;
}

vector<Vec2f> hough(int minVotes) {
	bool filtered = true;
	vector<Vec2f> lines;
	HoughLines(image, lines, 1, CV_PI/180, minVotes, 0, 0 );

	
	commands << "<command name=\"hough\" >" << endl;
	commands << "<param value=\"" << minVotes << "\" />" << endl;
	commands << "</command>" << endl;


	if (filtered) {
		vector<Vec2f> linesFilteredH;
		vector<Vec2f> linesFilteredV;
		for( size_t i = 0; i < lines.size(); i++ )
		{
			double degree = lines[i][1] * 180 / CV_PI;
			if (degree < 1 && degree > -1) {
				linesFilteredH.push_back(lines[i]);
			} else if (degree < 91 && degree > 89) {
				linesFilteredV.push_back(lines[i]);
			}
		}

		vector<Vec2f> linesGroupedH;
		for( size_t i = 0; i < linesFilteredH.size(); i++ ) {
			bool isInGroup = false;
			for( size_t j = 0; j < linesGroupedH.size(); j++ ) {
				if (linesFilteredH[i][0] < linesGroupedH[j][0] + 50 && linesFilteredH[i][0] > linesGroupedH[j][0] - 50) {
					linesGroupedH[j][0] = (linesGroupedH[j][0] + linesFilteredH[i][0]) / 2;
					linesGroupedH[j][1] = (linesGroupedH[j][1] + linesFilteredH[i][1]) / 2;
					isInGroup = true;
					j = linesGroupedH.size();
				}
			}
			if (!isInGroup) {
				linesGroupedH.push_back(linesFilteredH[i]);
			}
		}

		vector<Vec2f> linesGroupedV;
		for( size_t i = 0; i < linesFilteredV.size(); i++ ) {
			bool isInGroup = false;
			for( size_t j = 0; j < linesGroupedV.size(); j++ ) {
				if (linesFilteredV[i][0] < linesGroupedV[j][0] + 50 && linesFilteredV[i][0] > linesGroupedV[j][0] - 50) {
					linesGroupedV[j][0] = (linesGroupedV[j][0] + linesFilteredV[i][0]) / 2;
					linesGroupedV[j][1] = (linesGroupedV[j][1] + linesFilteredV[i][1]) / 2;
					isInGroup = true;
					j = linesGroupedV.size();
				}
			}
			if (!isInGroup) {
				linesGroupedV.push_back(linesFilteredV[i]);
			}
		}


		for( size_t i = 0; i < linesGroupedV.size(); i++ )
		{
			float rho = linesGroupedV[i][0], theta = linesGroupedV[i][1];
			Point pt1, pt2;
			double a = cos(theta), b = sin(theta);
			double x0 = a*rho, y0 = b*rho;
			pt1.x = cvRound(x0 + 5000*(-b));
			pt1.y = cvRound(y0 + 5000*(a));
			pt2.x = cvRound(x0 - 5000*(-b));
			pt2.y = cvRound(y0 - 5000*(a));
			line(image, pt1, pt2, Scalar(255,0,255), 3, CV_AA);
		}

		for( size_t i = 0; i < linesGroupedH.size(); i++ )
		{
			float rho = linesGroupedH[i][0], theta = linesGroupedH[i][1];
			Point pt1, pt2;
			double a = cos(theta), b = sin(theta);
			double x0 = a*rho, y0 = b*rho;
			pt1.x = cvRound(x0 + 5000*(-b));
			pt1.y = cvRound(y0 + 5000*(a));
			pt2.x = cvRound(x0 - 5000*(-b));
			pt2.y = cvRound(y0 - 5000*(a));
			line(image, pt1, pt2, Scalar(255,0,255), 3, CV_AA);
		}

		vector<Vec2f> returnLines;
		returnLines.reserve( linesGroupedH.size() + linesGroupedV.size() ); // preallocate memory
		returnLines.insert( returnLines.end(), linesGroupedH.begin(), linesGroupedH.end() );
		returnLines.insert( returnLines.end(), linesGroupedV.begin(), linesGroupedV.end() );
		return returnLines;



	} else {


		for( size_t i = 0; i < lines.size(); i++ )
		{
			float rho = lines[i][0], theta = lines[i][1];
			Point pt1, pt2;
			double a = cos(theta), b = sin(theta);
			double x0 = a*rho, y0 = b*rho;
			pt1.x = cvRound(x0 + 5000*(-b));
			pt1.y = cvRound(y0 + 5000*(a));
			pt2.x = cvRound(x0 - 5000*(-b));
			pt2.y = cvRound(y0 - 5000*(a));
			line(image, pt1, pt2, Scalar(255,0,255), 3, CV_AA);
		}
		return lines;
	}
}

vector<Vec4i> houghP(int minVotes) {
  vector<Vec4i> lines;
  HoughLinesP(image, lines, 1, CV_PI/180, minVotes, 50, 50 );
  for( size_t i = 0; i < lines.size(); i++ )
  {
    Vec4i l = lines[i];
    line( image, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255,255,255), 3, CV_AA);
  }

  	commands << "<command name=\"houghP\" >" << endl;
	commands << "<param value=\"" << minVotes << "\" />" << endl;
	commands << "</command>" << endl;

  return lines;
}

void hsvFilter(int a, int b, int c, int d, int e, int f) {
	Mat lower_red_hue_range;
  	inRange(image, Scalar(a, b, c), Scalar(d, e, f), lower_red_hue_range);
	image = lower_red_hue_range;

	a +=90;
	if(a > 180) {
		a -= 180;
	}

	d +=90;
	if (d > 180) {
		d -= 180;
	}

	
	commands << "<command name=\"hsvFilter\" >" << endl;
	commands << "<param value=\"" << a << "\" />" << endl;
	commands << "<param value=\"" << b << "\" />" << endl;
	commands << "<param value=\"" << c << "\" />" << endl;
	commands << "<param value=\"" << d << "\" />" << endl;
	commands << "<param value=\"" << e << "\" />" << endl;
	commands << "<param value=\"" << f << "\" />" << endl;
	commands << "</command>" << endl;
}

void detectBlobs() {
	Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create();
	
	std::vector<KeyPoint> keypoints;
	detector->detect( image, keypoints);

	Mat im_with_keypoints;
	image = imread(imagePath);
	drawKeypoints( image, keypoints, im_with_keypoints, Scalar(0,0,255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
	image = im_with_keypoints;

	commands << "<command name=\"detectBlobs\" />" << endl;
}

Point2f findCenter(bool all) {
	vector<vector<Point>> contours = contour(!all);

	vector<Moments> mu(contours.size() );
	for( int i = 0; i < contours.size(); i++ )
	{ 
		mu[i] = moments( contours[i], false ); 
	}

	vector<Point2f> mc( contours.size() );
	for( int i = 0; i < contours.size(); i++ )
	{ 
		mc[i] = Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 ); 
	}

	commands << "<command name=\"findCenter\" >" << endl;
	commands << "<param value=\"" << all << "\" />" << endl;
	commands << "</command>" << endl;
	return mc[0];
}

void crop() {
	int x1 = image.cols / 3;
	int y1 = 0;
	int x2 = image.cols / 3;
	int y2 = image.rows / 3 * 2;

	Rect myROI(x1, y1, x2, y2);
	image = image(myROI);
	commands << "<command name=\"crop\" />" << endl;
}


void shap() {
	Mat tmp;

    Mat kern = (Mat_<char>(3,3) <<  -1, -1,  -1,
                               -1,  9, -1,
                                -1, -1,  -1);
	filter2D(image, tmp, image.depth(), kern);
	image = tmp;

	commands << "<command name=\"shap\" />" << endl;
}

void leftRightFloodFill() {
	for (int i = 0; i < image.rows; i = i + 200) {
		Scalar border(8,8,8);
		floodFill(image, Point(1,i), Scalar(255,255,255), 0, border, border, 8);
		floodFill(image, Point(image.cols - 1,i), Scalar(255,255,255), 0, border, border, 8);
	}
	commands << "<command name=\"leftRightFloodFill\" />" << endl;
}

void matchMaxShape (vector<Point> shape)  {
	commands << "<command name=\"matchMaxShape\" >" << endl;
	commands << "<param value=\"" << shape << "\" />" << endl;
	commands << "</command>" << endl;
}
