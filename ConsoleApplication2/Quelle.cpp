//#include "opencv2/core.hpp"
//#include "opencv2/imgproc.hpp"
//#include <opencv2/highgui.hpp>
//#include "opencv2/features2d.hpp"
//#include "opencv2/xfeatures2d.hpp"
//#include "opencv2/calib3d.hpp"
//#include "opencv2/xfeatures2d/nonfree.hpp"
//#include <iostream>
//#include <stdlib.h>
//#include <vector>
//
////#define CONTOURS
////#define CANNY
//
//using namespace cv;
//using namespace cv::xfeatures2d;
//using namespace std;
//
///// Global variables
//Mat src, src_gray;
//int thresh = 70	;
//int max_thresh = 255;
//
//char* source_window = "Source image";
//char* corners_window = "Corners detected";
//
////int maxCorners = 300;
////int maxTrackbar = 1000;
//
//int maxCorners = 10;
//int maxTrackbar = 25;
//
//RNG rng(12345);
//
///// Function header
//void cornerHarris_demo( int, void* );
//void goodFeaturesToTrack_Demo( int, void* );
//void goodFeaturesToTrack_Demo2( int, void* );
//void Harris();
//void ShiTomasi();
//void subpixeles();
//void surf();
//void match();
//void flannTest();
//void AKAZETest();
//void MSERTest();
//void circle();
//void cannyTest();
//void sortCorners(std::vector<cv::Point2f>& corners, cv::Point2f center);
//Point2f computeIntersect(Vec4i a, Vec4i b);
//void rectangeTest();
//
////int main( int argc, const char* argv[] ) {
//	//Harris();
//	//ShiTomasi();
//	//subpixeles();
//	//surf();
//	//match();
//	//flannTest();
//	//AKAZETest();
//	//MSERTest();
//	//circle();
//	//cannyTest();
//	//rectangeTest();
////}
//
//
//cv::Point2f center(0,0);
//void rectangeTest() {
//	Mat src = imread( "2.jpg" );
//
//	Mat bw;
//	cvtColor(src, bw, CV_BGR2GRAY);
//
//	equalizeHist( bw, bw );
//
//	blur(bw, bw, cv::Size(3, 3));
//
//	//equalizeHist( bw, bw );
//
//	Mat canny_output;
//
//
//	/// Detect edges using canny
//	int thresh = 75;
//	Canny( bw, canny_output, thresh, thresh*4, 3 );
//	
//	
//	
//	/// Find contours
//	/*vector<vector<Point> > contours;
//	vector<Vec4i> hierarchy;
//	findContours( canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
//
//	 Mat drawing = Mat::zeros( canny_output.size(), CV_8UC3 );
//	for( int i = 0; i< contours.size(); i++ )
//     {
//       Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
//       drawContours( drawing, contours, i, color, 1, 8, hierarchy, 0, Point() );
//     }*/
//
//	imshow("out", canny_output);
//
//	cv::waitKey();
//}
//
//void cannyTest() {
//	Mat org = imread("1.jpg");
//
//	Mat orgGray;
//	cvtColor(org, orgGray, CV_RGB2GRAY);
//	GaussianBlur(orgGray, orgGray, Size(3,3), 0);
//
//	int a = 100;
//	Canny(orgGray, orgGray, a, a*3, 3);
//
//	vector<Vec3f> circles;
//
//  /// Apply the Hough Transform to find the circles
//  HoughCircles( orgGray, circles, HOUGH_GRADIENT, 1, src_gray.rows/8,200, 100, 0, 0 );
//
//  /// Draw the circles detected
//  for( size_t i = 0; i < circles.size(); i++ )
//  {
//      Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
//      int radius = cvRound(circles[i][2]);
//      // circle center
//      circle( orgGray, center, 3, Scalar(0,255,0), -1, 8, 0 );
//      // circle outline
//      circle( orgGray, center, radius, Scalar(0,0,255), 3, 8, 0 );
//   }
//
//  imshow( "Hough Circle Transform Demo", orgGray );
//
//  waitKey(0);
//}
//
//void circle() {
//	Mat src_gray = imread( "1.jpg", IMREAD_GRAYSCALE );
//
//	equalizeHist( src_gray, src_gray );
//
//  /// Reduce the noise so we avoid false circle detection
//  GaussianBlur( src_gray, src_gray, Size(5, 5), 2, 2 );
//
//  vector<Vec3f> circles;
//
//  /// Apply the Hough Transform to find the circles
//  HoughCircles( src_gray, circles, HOUGH_GRADIENT, 1, src_gray.rows/8,200, 100, 0, 0 );
//
//  /// Draw the circles detected
//  for( size_t i = 0; i < circles.size(); i++ )
//  {
//      Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
//      int radius = cvRound(circles[i][2]);
//      // circle center
//      circle( src_gray, center, 3, Scalar(0,255,0), -1, 8, 0 );
//      // circle outline
//      circle( src_gray, center, radius, Scalar(0,0,255), 3, 8, 0 );
//   }
//
//  /// Show your results
//  namedWindow( "Hough Circle Transform Demo", WINDOW_AUTOSIZE );
//  imshow( "Hough Circle Transform Demo", src_gray );
//
//  waitKey(0);
//}
//
//
//cv::Point2f computeIntersect(cv::Vec4i a, 
//                             cv::Vec4i b)
//{
//	int x1 = a[0], y1 = a[1], x2 = a[2], y2 = a[3], x3 = b[0], y3 = b[1], x4 = b[2], y4 = b[3];
//	float denom;
//
//	if (float d = ((float)(x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4)))
//	{
//		cv::Point2f pt;
//		pt.x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / d;
//		pt.y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / d;
//		return pt;
//	}
//	else
//		return cv::Point2f(-1, -1);
//}
//
//void sortCorners(std::vector<cv::Point2f>& corners, 
//                 cv::Point2f center)
//{
//	std::vector<cv::Point2f> top, bot;
//
//	for (int i = 0; i < corners.size(); i++)
//	{
//		if (corners[i].y < center.y)
//			top.push_back(corners[i]);
//		else
//			bot.push_back(corners[i]);
//	}
//	corners.clear();
//	
//	if (top.size() == 2 && bot.size() == 2){
//		cv::Point2f tl = top[0].x > top[1].x ? top[1] : top[0];
//		cv::Point2f tr = top[0].x > top[1].x ? top[0] : top[1];
//		cv::Point2f bl = bot[0].x > bot[1].x ? bot[1] : bot[0];
//		cv::Point2f br = bot[0].x > bot[1].x ? bot[0] : bot[1];
//	
//		
//		corners.push_back(tl);
//		corners.push_back(tr);
//		corners.push_back(br);
//		corners.push_back(bl);
//	}
//}
//
//void MSERTest() {
//	Mat box = imread("6.jpg",1);
//	vector<vector<Point>> regions;
//    Ptr<MSER> ms = MSER::create();
//	ms->detectRegions(box, regions, vector< Rect >());
//
//    for (int i = 0; i < regions.size(); i++)
//    {
//        ellipse(box, fitEllipse(regions[i]), Scalar(255));
//    }
//    imshow("mser", box);
//    waitKey(0);
//}
//
//const float inlier_threshold = 2.5f; // Distance threshold to identify inliers
//const float nn_match_ratio = 0.8f;   // Nearest neighbor matching ratio
//
//void AKAZETest() {
//
//	Mat img1 = imread( "image008.jpg", IMREAD_GRAYSCALE );
//	Mat img2 = imread( "image009.jpg", IMREAD_GRAYSCALE );
//
//	//-- Step 1: Detect the keypoints using SURF Detector, compute the descriptors
//	int minHessian = 400;
//
//	Ptr<SURF> detector = SURF::create(minHessian);
//
//	std::vector<KeyPoint> keypoints_1, keypoints_2;
//	Mat descriptors_1, descriptors_2;
//
//	detector->detectAndCompute( img1, Mat(), keypoints_1, descriptors_1 );
//	detector->detectAndCompute( img2, Mat(), keypoints_2, descriptors_2 );
//
//	//-- Step 3: Matching descriptor vectors using FLANN matcher
//	FlannBasedMatcher matcher;
//	std::vector< DMatch > matches;
//	matcher.match( descriptors_1, descriptors_2, matches );
//
//	double max_dist = 0; double min_dist = 100;
//
//	//-- Quick calculation of max and min distances between keypoints
//	for( int i = 0; i < descriptors_1.rows; i++ )
//	{ double dist = matches[i].distance;
//		if( dist < min_dist ) min_dist = dist;
//		if( dist > max_dist ) max_dist = dist;
//	}
//
//	printf("-- Max dist : %f \n", max_dist );
//	printf("-- Min dist : %f \n", min_dist );
//
//	//-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist,
//	//-- or a small arbitary value ( 0.02 ) in the event that min_dist is very
//	//-- small)
//	//-- PS.- radiusMatch can also be used here.
//	std::vector< DMatch > good_matches;
//
//	for( int i = 0; i < descriptors_1.rows; i++ ) { 
//		if( matches[i].distance <= max(2*min_dist, 0.02) ) { 
//			good_matches.push_back( matches[i]); 
//		}
//	}
//
//	//-- Localize the object
//	std::vector<Point2f> obj;
//	std::vector<Point2f> scene;
//
//	for( int i = 0; i < good_matches.size(); i++ )
//	{
//		//-- Get the keypoints from the good matches
//		obj.push_back( keypoints_1[ good_matches[i].queryIdx ].pt );
//		scene.push_back( keypoints_2[ good_matches[i].trainIdx ].pt );
//	}
//
//	Mat homography = findHomography( obj, scene, RANSAC);
//
//    vector<KeyPoint> kpts1, kpts2;
//    Mat desc1, desc2;
//
//    Ptr<AKAZE> akaze = AKAZE::create();
//    akaze->detectAndCompute(img1, noArray(), kpts1, desc1);
//    akaze->detectAndCompute(img2, noArray(), kpts2, desc2);
//
//    BFMatcher matcher2(NORM_HAMMING);
//    vector< vector<DMatch> > nn_matches;
//    matcher2.knnMatch(desc1, desc2, nn_matches, 2);
//
//    vector<KeyPoint> matched1, matched2, inliers1, inliers2;
//    vector<DMatch> good_matches2;
//    for(size_t i = 0; i < nn_matches.size(); i++) {
//        DMatch first = nn_matches[i][0];
//        float dist1 = nn_matches[i][0].distance;
//        float dist2 = nn_matches[i][1].distance;
//
//        if(dist1 < nn_match_ratio * dist2) {
//            matched1.push_back(kpts1[first.queryIdx]);
//            matched2.push_back(kpts2[first.trainIdx]);
//        }
//    }
//
//    for(unsigned i = 0; i < matched1.size(); i++) {
//        Mat col = Mat::ones(3, 1, CV_64F);
//        col.at<double>(0) = matched1[i].pt.x;
//        col.at<double>(1) = matched1[i].pt.y;
//
//        col = homography * col;
//        col /= col.at<double>(2);
//        double dist = sqrt( pow(col.at<double>(0) - matched2[i].pt.x, 2) +
//                            pow(col.at<double>(1) - matched2[i].pt.y, 2));
//
//        if(dist < inlier_threshold) {
//            int new_i = static_cast<int>(inliers1.size());
//            inliers1.push_back(matched1[i]);
//            inliers2.push_back(matched2[i]);
//            good_matches2.push_back(DMatch(new_i, new_i, 0));
//        }
//    }
//
//    Mat res;
//    drawMatches(img1, inliers1, img2, inliers2, good_matches2, res);
//    imwrite("res.png", res);
//
//    double inlier_ratio = inliers1.size() * 1.0 / matched1.size();
//    cout << "A-KAZE Matching Results" << endl;
//    cout << "*******************************" << endl;
//    cout << "# Keypoints 1:                        \t" << kpts1.size() << endl;
//    cout << "# Keypoints 2:                        \t" << kpts2.size() << endl;
//    cout << "# Matches:                            \t" << matched1.size() << endl;
//    cout << "# Inliers:                            \t" << inliers1.size() << endl;
//    cout << "# Inliers Ratio:                      \t" << inlier_ratio << endl;
//    cout << endl;
//
//}
//
//void flannTest() {
//	  Mat img_1 = imread( "r1.jpg", IMREAD_GRAYSCALE );
//  Mat img_2 = imread( "r3.jpg", IMREAD_GRAYSCALE );
//  //Mat img_1;
//  //Mat img_2;
//
//  //equalizeHist( img1, img_1 );
//  //equalizeHist( img2, img_2 );
//
////-- Step 1: Detect the keypoints using SURF Detector, compute the descriptors
//  int minHessian = 1000;
//
//  Ptr<SURF> detector = SURF::create();
//  //Ptr<SIFT> detector = SIFT::create();
//
//  std::vector<KeyPoint> keypoints_1, keypoints_2;
//  Mat descriptors_1, descriptors_2;
//
//  detector->detectAndCompute( img_1, Mat(), keypoints_1, descriptors_1 );
//  detector->detectAndCompute( img_2, Mat(), keypoints_2, descriptors_2 );
//
//  //-- Step 3: Matching descriptor vectors using FLANN matcher
//  FlannBasedMatcher matcher;
//  std::vector< DMatch > matches;
//  matcher.match( descriptors_1, descriptors_2, matches );
//   	//std::vector< std::vector< DMatch > >  good_matches;
//  //matcher.radiusMatch(descriptors_1, descriptors_2, good_matches, 0.159594);
//
//
//  double max_dist = 0; 
//  double min_dist = 100;
//
//  //-- Quick calculation of max and min distances between keypoints
//  for( int i = 0; i < descriptors_1.rows; i++ )
//  { double dist = matches[i].distance;
//    if( dist < min_dist ) min_dist = dist;
//    if( dist > max_dist ) max_dist = dist;
//  }
//
//  printf("-- Max dist : %f \n", max_dist );
//  printf("-- Min dist : %f \n", min_dist );
//
//  //-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist,
//  //-- or a small arbitary value ( 0.02 ) in the event that min_dist is very
//  //-- small)
//  //-- PS.- radiusMatch can also be used here.
//  std::vector< DMatch > good_matches;
//  for( int i = 0; i < descriptors_1.rows; i++ ) { 
//	  if( matches[i].distance <= max(2*min_dist, 0.02) ) {
//		  good_matches.push_back( matches[i]); 
//	  }
//  }
//
//  //-- Draw only "good" matches
//  Mat img_matches;
//  drawMatches( img_1, 
//				keypoints_1, 
//				img_2, 
//				keypoints_2,
//                good_matches, 
//				img_matches, 
//				Scalar::all(-1), 
//				Scalar::all(-1),
//                std::vector< char >(), 
//				DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
//
//  //-- Show detected matches
//  imshow( "Good Matches", img_matches );
//
//  //imwrite("9.png", img_matches);
//
// /* for( int i = 0; i < (int)good_matches.size(); i++ )
//  { printf( "-- Good Match [%d] Keypoint 1: %d  -- Keypoint 2: %d  \n", i, good_matches[i].queryIdx, good_matches[i].trainIdx ); }
//*/
//
//  waitKey(0);
//}
//
//void match() {
//  Mat img_1 = imread( "1.jpg", IMREAD_GRAYSCALE );
//  Mat img_2 = imread( "2.jpg", IMREAD_GRAYSCALE );
//
//
//  //-- Step 1: Detect the keypoints using SURF Detector, compute the descriptors
//  int minHessian = 20000;
//
//  Ptr<SURF> detector = SURF::create(minHessian );
//
//  std::vector<KeyPoint> keypoints_1, keypoints_2;
//  Mat descriptors_1, descriptors_2;
//
//  //detector->detect( img_1, keypoints_1 );
//  //detector->detect( img_2, keypoints_2 );
//
//  detector->detectAndCompute( img_1, Mat(), keypoints_1, descriptors_1 );
//  detector->detectAndCompute( img_2, Mat(), keypoints_2, descriptors_2 );
//
//  //-- Step 2: Matching descriptor vectors with a brute force matcher
//  BFMatcher matcher(NORM_L2);
//  std::vector< DMatch > matches;
//  matcher.match( descriptors_1, descriptors_2, matches );
//
//  //-- Draw matches
//   Mat img_matches;
//  drawMatches( img_1, keypoints_1, img_2, keypoints_2, matches, img_matches );
//
//  //-- Show detected matches
//  imshow("Matches", img_matches );
//
//  waitKey(0);
//
//}
//
//void surf() {
//  Mat img_1 = imread( "image008.jpg", IMREAD_GRAYSCALE );
//  Mat img_2 = imread( "image008.jpg", IMREAD_GRAYSCALE );
//
//  //-- Step 1: Detect the keypoints using SURF Detector
//  int minHessian = 400;
//
//  //Ptr<SURF> detector = SURF::create( minHessian );
//  Ptr<xfeatures2d::SURF> detector = xfeatures2d::SURF::create();
//
//  vector<KeyPoint> keypoints_1, keypoints_2;
//
//  detector->detect( img_1, keypoints_1 );
//  detector->detect( img_2, keypoints_2 );
//
//  //-- Draw keypoints
//  Mat img_keypoints_1; Mat img_keypoints_2;
//
//  drawKeypoints( img_1, keypoints_1, img_keypoints_1, Scalar::all(-1), DrawMatchesFlags::DEFAULT );
//  drawKeypoints( img_2, keypoints_2, img_keypoints_2, Scalar::all(-1), DrawMatchesFlags::DEFAULT );
//
//  //-- Show detected (drawn) keypoints
//  imshow("Keypoints 1", img_keypoints_1 );
//  imshow("Keypoints 2", img_keypoints_2 );
//
//
//  waitKey(0);
//}
//
//void subpixeles() {
//	/// Load source image and convert it to gray
//  src = imread("1a.jpg", 1 );
//  cvtColor( src, src_gray, COLOR_BGR2GRAY );
//
//  /// Create Window
//  namedWindow( source_window, WINDOW_AUTOSIZE );
//
//  /// Create Trackbar to set the number of corners
//  createTrackbar( "Max  corners:", source_window, &maxCorners, maxTrackbar, goodFeaturesToTrack_Demo2);
//
//  imshow( source_window, src );
//
//
//  goodFeaturesToTrack_Demo( 0, 0 );
//
//  waitKey(0);
//
//}
//
//void ShiTomasi() {
//	/// Load source image and convert it to gray
//  src = imread("1a.jpg", 1 );
//  cvtColor( src, src_gray, COLOR_BGR2GRAY );
//
//  /// Create Window
//  namedWindow( source_window, WINDOW_AUTOSIZE );
//
//  /// Create Trackbar to set the number of corners
//  createTrackbar( "Max  corners:", source_window, &maxCorners, maxTrackbar, goodFeaturesToTrack_Demo );
//
//  imshow( source_window, src );
//
//  goodFeaturesToTrack_Demo( 0, 0 );
//
//  waitKey(0);
//}
//
//void Harris() {
//  /// Load source image and convert it to gray
//  src = imread("image008.jpg", 1 );
//  cvtColor( src, src_gray, COLOR_BGR2GRAY );
//
//  /// Create a window and a trackbar
//  namedWindow( source_window, WINDOW_AUTOSIZE );
//  createTrackbar( "Threshold: ", source_window, &thresh, max_thresh, cornerHarris_demo );
//  imshow( source_window, src );
//
//  cornerHarris_demo( 0, 0 );
//
//  waitKey(0);
//}
//
///** @function cornerHarris_demo */
//void cornerHarris_demo( int, void* )
//{
//
//  Mat dst, dst_norm, dst_norm_scaled;
//  dst = Mat::zeros( src.size(), CV_32FC1 );
//
//  /// Detector parameters
//  int blockSize = 2;
//  int apertureSize = 3;
//  double k = 0.04;
//
//  /// Detecting corners
//  cornerHarris( src_gray, dst, blockSize, apertureSize, k, BORDER_DEFAULT );
//
//  /// Normalizing
//  normalize( dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );
//  convertScaleAbs( dst_norm, dst_norm_scaled );
//
//  /// Drawing a circle around corners
//  for( int j = 0; j < dst_norm.rows ; j++ )
//     { for( int i = 0; i < dst_norm.cols; i++ )
//          {
//            if( (int) dst_norm.at<float>(j,i) > thresh )
//              {
//               circle( dst_norm_scaled, Point( i, j ), 5,  Scalar(0), 2, 8, 0 );
//              }
//          }
//     }
//  /// Showing the result
//  namedWindow( corners_window, WINDOW_AUTOSIZE );
//  imshow( corners_window, dst_norm_scaled );
//}
//
//void goodFeaturesToTrack_Demo( int, void* )
//{
//  if( maxCorners < 1 ) { maxCorners = 1; }
//
//  /// Parameters for Shi-Tomasi algorithm
//  vector<Point2f> corners;
//  double qualityLevel = 0.01;
//  double minDistance = 10;
//  int blockSize = 3;
//  bool useHarrisDetector = false;
//  double k = 0.04;
//
//  /// Copy the source image
//  Mat copy;
//  copy = src.clone();
//
//  /// Apply corner detection
//  goodFeaturesToTrack( src_gray,
//               corners,
//               maxCorners,
//               qualityLevel,
//               minDistance,
//               Mat(),
//               blockSize,
//               useHarrisDetector,
//               k );
//
//
//  /// Draw corners detected
//  cout<<"** Number of corners detected: "<<corners.size()<<endl;
//  int r = 4;
//  for( int i = 0; i < corners.size(); i++ )
//     { circle( copy, corners[i], r, Scalar(rng.uniform(0,255), rng.uniform(0,255),
//              rng.uniform(0,255)), -1, 8, 0 ); }
//
//  /// Show what you got
//  namedWindow( source_window, WINDOW_AUTOSIZE );
//  imshow( source_window, copy );
//}
//
//void goodFeaturesToTrack_Demo2( int, void* )
//{
//  if( maxCorners < 1 ) { maxCorners = 1; }
//
//  /// Parameters for Shi-Tomasi algorithm
//  vector<Point2f> corners;
//  double qualityLevel = 0.01;
//  double minDistance = 10;
//  int blockSize = 3;
//  bool useHarrisDetector = false;
//  double k = 0.04;
//
//  /// Copy the source image
//  Mat copy;
//  copy = src.clone();
//
//  /// Apply corner detection
//  goodFeaturesToTrack( src_gray,
//               corners,
//               maxCorners,
//               qualityLevel,
//               minDistance,
//               Mat(),
//               blockSize,
//               useHarrisDetector,
//               k );
//
//
//  /// Draw corners detected
//  cout<<"** Number of corners detected: "<<corners.size()<<endl;
//  int r = 4;
//  for( int i = 0; i < corners.size(); i++ )
//     { circle( copy, corners[i], r, Scalar(rng.uniform(0,255), rng.uniform(0,255),
//                                                 rng.uniform(0,255)), -1, 8, 0 ); }
//
//  /// Show what you got
//  namedWindow( source_window, WINDOW_AUTOSIZE );
//  imshow( source_window, copy );
//
//  /// Set the neeed parameters to find the refined corners
//  Size winSize = Size( 5, 5 );
//  Size zeroZone = Size( -1, -1 );
//  TermCriteria criteria = TermCriteria( TermCriteria::EPS + TermCriteria::MAX_ITER, 40, 0.001 );
//
//  /// Calculate the refined corner locations
//  cornerSubPix( src_gray, corners, winSize, zeroZone, criteria );
//
//  /// Write them down
//  for( int i = 0; i < corners.size(); i++ )
//     { cout<<" -- Refined Corner ["<<i<<"]  ("<<corners[i].x<<","<<corners[i].y<<")"<<endl; }
//}
//
///**int main( int argc, const char* argv[] )
//{
//	RNG rng(12345);
//
//	Mat org = imread("1.jpg");
//	Mat view = imread("2.JPG");
//
//	Mat orgGray;
//	cvtColor(org, orgGray, CV_RGB2GRAY);
//
//	Mat viewGray;
//	cvtColor(view, viewGray, CV_RGB2GRAY);
//
//	GaussianBlur(orgGray, orgGray, Size(3,3), 0);
//	GaussianBlur(viewGray, viewGray, Size(3,3), 0);
//
//#ifdef CANNY	
//	int a = 160;
//	Canny(orgGray, orgGray, a, a*3, 3);
//	Canny(viewGray, viewGray, a, a*3, 3);
//#endif	
//
//#ifdef LAPLACIAN
//	Mat org2;
//	Laplacian(orgGray, org2, CV_16S, 3, 1, 70, BORDER_DEFAULT );
//	Mat view2;
//	Laplacian(viewGray, view2, CV_16S, 3, 1, 70, BORDER_DEFAULT);
//
//	convertScaleAbs( org2, orgGray );
//	convertScaleAbs( view2, viewGray );
//#endif
//	threshold(orgGray, orgGray, 150, 255, THRESH_TOZERO);
//	//threshold(orgGray, orgGray, 175, 255, THRESH_TOZERO_INV);
//	threshold(viewGray, viewGray, 150, 255, THRESH_BINARY);
//
//	Mat element = getStructuringElement(0,
//                                       Size( 2*1 + 1, 2*1+1 ),
//                                       Point( 1, 1 ) );
//
//	Mat temp;
//	//dilate(orgGray, temp, element);
//	//erode(temp, orgGray, 0);
//	//dilate(temp, orgGray, 0);
//
//	
//	imshow("org", orgGray);
//	imshow("view", viewGray);
//	waitKey(0);
//
//#ifdef CONTOURS
//	vector<vector<Point> > contoursOrg;
//	vector<Vec4i> hierarchyOrg;
//	findContours(orgGray, contoursOrg, hierarchyOrg, CV_RETR_TREE, CV_CHAIN_APPROX_TC89_KCOS, Point(0, 0) );
//	Mat orgOut = Mat::zeros( orgGray.size(), CV_8UC3 );
//	drawContours(orgOut, contoursOrg, -1, Scalar(255, 0, 0));
//
//	vector<vector<Point> > contoursView;
//	vector<Vec4i> hierarchyView;
//	findContours( viewGray, contoursView, hierarchyView, CV_RETR_TREE, CV_CHAIN_APPROX_TC89_KCOS, Point(0, 0) );
//	Mat viewOut = Mat::zeros( viewGray.size(), CV_8UC3 );
//	drawContours(viewOut, contoursView, -1, Scalar(255, 0, 0));
//
//	Mat out = Mat::zeros( viewGray.size(), CV_8UC3 );
//	for (int i = 0; i < contoursOrg.size(); i++) {
//		vector<Point>& a = contoursOrg[i];	
//		double maxMatch = 0;
//		int maxIndex = 0;
//		//out = Mat::zeros( viewGray.size(), CV_8UC3 );
//		double test = contourArea(contoursOrg[i]);
//		if (test > 400) {
//			for (int idx = 0; idx < contoursView.size(); idx++) {
//				double test2 = contourArea(contoursView[idx]);
//				if (test2 > 400) {
//					vector<Point>& b = contoursView[idx];
//					double match = matchShapes(a, b, CV_CONTOURS_MATCH_I1, 0);	 
//					if(match > 10) {
//						Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
//						drawContours(out, contoursView, idx, color);
//						drawContours(out, contoursOrg, i, color);
//						imshow("match", out);
//						waitKey(0);
//					}
//				}
//			}
//		}
//		
//	}
//
//	imshow("eins", orgOut);
//	imshow("zwei", viewOut);
//	imshow("match", out);
//#endif
//#ifdef LINES
//	Mat out = Mat::zeros( orgGray.size(), CV_8UC3 );
//
//	vector<Vec4i> linesOrg;
//	HoughLinesP(orgGray, linesOrg, 1, CV_PI/180, 100, 0, 7 );
//	for( size_t i = 0; i < linesOrg.size(); i++ )
//	{
//		Vec4i l = linesOrg[i];
//		line( org, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 1, CV_AA);
//	}
//	vector<Vec4i> linesView;
//	HoughLinesP(viewGray, linesView, 1, CV_PI/180, 100, 0, 7 );
//	for( size_t i = 0; i < linesView.size(); i++ )
//	{
//		Vec4i l = linesView[i];
//		line( view, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255,0,0), 1, CV_AA);
//	}
//
//	//imshow("lines", out);
//	namedWindow("org");
//	namedWindow("view");
//	imshow("org", org);
//	imshow("view", view);
//#endif
//
//
//
//	
//
//	waitKey(0);
//
//}*/