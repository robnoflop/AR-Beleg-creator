//#include <stdio.h>
//#include <iostream>
//#include <stdio.h>
//#include <iostream>
//#include <sstream>
//#include "opencv2/core.hpp"
//#include "opencv2/features2d.hpp"
//#include "opencv2/imgcodecs.hpp"
//#include "opencv2/highgui.hpp"
//#include "opencv2/xfeatures2d.hpp"
//#include <opencv2/opencv.hpp>
//
//
//using namespace std;
//using namespace cv;
//using namespace cv::xfeatures2d;
//
//void flannTest();
//void mser();
//void WriteSIFTs(std::vector<cv::KeyPoint> &keys, cv::Mat desc, std::stringstream &out1);
//RNG rng(12345);
//
//int main(int argc, char *argv[]) {
//
//	//mser();
//
//	flannTest();
//
//	/*waitKey(0);
//
//	Mat src = imread("image009.jpg");
//	Mat src = imread("hb.jpg");
//	imshow("src", src);
//
//	Mat gray;
//	cvtColor( src, gray, CV_BGR2GRAY );*/
//	
//	//Mat blur;
//	//GaussianBlur( gray, blur, Size(3,3), 0);
//
//	//Mat equHist;
//	////equalizeHist( blur, equHist );
//
//	//Mat canny;
//	//int cannyLowThreshold = 25;
//	//int cannyKernelSize = 3;
//	//int cannyRatio = 3;
//	//Canny( blur, canny, cannyLowThreshold, cannyLowThreshold*cannyRatio, cannyKernelSize );
//
//
//	//Mat thres;
//	//threshold( blur, thres, 128, 255,THRESH_BINARY_INV );
//
//	//imshow("thres", thres);
//
//	////Mat hough = src.clone();
//	/*vector<Vec2f> lines;
//    HoughLines( canny, lines, 1, CV_PI/180, 100 );
//
//    for( size_t i = 0; i < lines.size(); i++ )
//    {
//        float rho = lines[i][0];
//        float theta = lines[i][1];
//		if (theta < 0.01 || (theta > 1.5 && theta < 1.6)) {
//			double a = cos(theta), b = sin(theta);
//			double x0 = a*rho, y0 = b*rho;
//			Point pt1(cvRound(x0 + 1000*(-b)),
//					  cvRound(y0 + 1000*(a)));
//			Point pt2(cvRound(x0 - 1000*(-b)),
//					  cvRound(y0 - 1000*(a)));
//			line( hough, pt1, pt2, Scalar(0,0,255), 1, 8 );
//		}
//	}*/
//
//	/*vector<Vec4i> lines;
//    HoughLinesP(canny, lines, 1, CV_PI/180, 50, 50, 10 );
//	int max = 0;
//    for( size_t i = 0; i < lines.size(); i++ )
//    {
//		Vec4i l = lines[i];
//		line( hough, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 1, CV_AA);
//    }*/
//
//	//imshow("hough", hough);
//
//
//	//vector<vector<Point> > contours;
// //   vector<Vec4i> hierarchy;
//	//findContours( thres, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
//
//	///// Draw contours
//	//Mat contur = Mat::zeros( canny.size(), CV_8UC3 );
//	//int maxSize = 0;
//	//int maxIndex = 0;
//	//for( int i = 0; i< contours.size(); i++ )
// //   {
//	//	if (maxSize < contourArea(contours[i])) {
//	//		maxSize = contourArea(contours[i]);
//	//		maxIndex = i;
//	//	}
// //   }
//
//	//Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
// //   drawContours( contur, contours, maxIndex, color, -1, 8);//, hierarchy, 2, Point() );
//
//	//imshow("conturs", contur);
//
//	//waitKey(0);
//}
//
//#define orb
//
//void flannTest() {
//	
//	Mat img_1 = imread( "_arena/test/01.jpg", IMREAD_GRAYSCALE );
//	Mat img_2 = imread( "_arena/test/09.jpg", IMREAD_GRAYSCALE );
//	
//
//
//#ifdef orb
//	Ptr<ORB> detector = ORB::create(4000);
//#endif
//#ifdef sift
//	Ptr<SIFT> detector = SIFT::create();
//#endif
//	std::vector<KeyPoint> keypoints_1, keypoints_2;
//	Mat descriptors_1, descriptors_2;
//
//	detector->detectAndCompute( img_1, Mat(), keypoints_1, descriptors_1 );
//	detector->detectAndCompute( img_2, Mat(), keypoints_2, descriptors_2 );
//
//#ifdef orb
//	BFMatcher matcher = BFMatcher(NORM_HAMMING, true);
//#endif
//#ifdef sift
//	FlannBasedMatcher  matcher;
//#endif
//
//	std::vector< DMatch > matches;
//	/*if(descriptors_1.type()!=CV_32F) {
//		descriptors_1.convertTo(descriptors_1, CV_32F);
//	}
//
//	if(descriptors_2.type()!=CV_32F) {
//		descriptors_2.convertTo(descriptors_2, CV_32F);
//	}*/
//	matcher.match( descriptors_1, descriptors_2, matches );
//
//	/*FileStorage fs("Keypoints.xml", FileStorage::WRITE);
//	write(fs, "descriptors", descriptors_1);
//	fs.release();
//	imwrite("descriptor.png", descriptors_1);*/
//
//	double max_dist = 0; 
//	double min_dist = 100;
//
//	//-- Quick calculation of max and min distances between keypoints
//	for( int i = 0; i < matches.size(); i++ ) {
//		double dist = matches[i].distance;
//		if( dist < min_dist ) min_dist = dist;
//		if( dist > max_dist ) max_dist = dist;
//	}
//
//	printf("-- Max dist : %f \n", max_dist );
//	printf("-- Min dist : %f \n", min_dist );
//
//	//-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist,
//	//-- or a small arbitary value ( 0.02 ) in the event that min_dist is very
//	//-- small)
//	//-- PS.- radiusMatch can also be used here.
//	std::vector< DMatch > good_matches;
//	for( int i = 0; i <  matches.size(); i++ ) { 
//		if( matches[i].distance <= max(3*min_dist, 0.02) ) {
//			good_matches.push_back( matches[i]); 
//		}
//	}
//
//	//-- Draw only "good" matches
//	Mat img_matches;
//	drawMatches( img_1, keypoints_1, img_2, keypoints_2, good_matches, img_matches, Scalar::all(-1), Scalar::all(-1), std::vector< char >(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
//
//	//-- Show detected matches
//	imshow( "Good Matches", img_matches );
//	imwrite("gkmatch.jpg", img_matches);
//
//	waitKey(0);
//}
//
//void mser() {
//	Mat image = imread("turm/01.jpg");
//	
//	Ptr<MSER> mser = MSER::create(200, 60, 50000);
//	vector<vector<Point>> regions;
//	vector<Rect> boxes;
//	mser->detectRegions(image, regions, boxes);
//
//	Mat drawing = Mat::zeros( image.size(), CV_8UC3 );
//	for (int i = 0; i < regions.size(); i++) {
//		drawContours( image, regions, i, Scalar(255,255,255), -1, 8);
//	}
//
//	resize(image, image, Size(800,800));
//	imshow("mser", image);
//
//	waitKey(0);
//}