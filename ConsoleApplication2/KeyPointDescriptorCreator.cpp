//#include <stdio.h>
//#include <iostream>
//#include <stdio.h>
//#include <iostream>
//#include <sstream>
//#include "opencv2/core.hpp"
//#include "opencv2/features2d.hpp"
//#include "opencv2/imgcodecs.hpp"
//#include "opencv2/highgui.hpp"
//#include <opencv2/opencv.hpp>
//
//#include"ContourMatchingDescriptor.h"
//
//using namespace std;
//using namespace cv;
//
//RNG rng(12345);
//
//
//
//bool myfunction(KeyPoint i, KeyPoint j) { return (i.response > j.response); }
//std::vector<cv::Point> getMaxContour(Mat img, string name);
//
//int main(int argc, char *argv[]) {
//	cv::String path("_arena/*.jpg"); //select only jpg
//	vector<cv::String> fn;
//	vector<cv::Mat> data;
//	cv::glob(path,fn,true); // recurse
//	Ptr<ORB> detector = ORB::create(4000); // , 1.2, 16, 62, 0, 3, ORB::HARRIS_SCORE, 62, 20);
//
//	//FileStorage fs("orbParams.yml", FileStorage::WRITE);
//	//fs << detector;
//	//
//
//	std::string names[] = { "01",
//		"02",
//		"03",
//		"04",
//		"05",
//		"06",
//		"07",
//		"08",
//		"09",
//		"10",
//		"11",
//		"12",
//		"13",
//		"14",
//		"15",
//		"16",
//		"17",
//		"18",
//		"19",
//		"20",
//		"21",
//		"22",
//		"23",
//		"24",
//		"25",
//		"26",
//		"27",
//		"28",
//		"31",
//		"32",
//		"33",
//		"34",
//		"35",
//		"36",
//		"37",
//		"38",
//		"39",
//		"40",
//		"41",
//		"42",
//		"43" };
//
//	for (std::string imageNumber : names)
//	{
//		//cv::Mat im = cv::imread(fn[k]);
//		//if (im.empty()) continue; //only proceed if sucsessful
//		 
//		std::string object = "_arena";
//
//		/*cv::Mat im = cv::imread(object + "/" + imageNumber + ".jpg");
//		std::vector<KeyPoint> keypoints;
//		Mat descriptors;
//		detector->detectAndCompute(im, Mat(), keypoints, descriptors);
//		std::vector<cv::Point> helpContour;
//		ContourMatchingDescriptor descriptor = ContourMatchingDescriptor::buildFromKeyPoints(helpContour, im.cols, im.rows, keypoints);*/
//
//		cv::Mat im_ref = cv::imread(object + "/ref/" + imageNumber + ".jpg");
//		std::vector<KeyPoint> keypoints_ref;
//		Mat descriptors_ref;
//		detector->detectAndCompute(im_ref, Mat(), keypoints_ref, descriptors_ref);
//		//detector->write("detectorSettings.yml");
//		//break;
//		stringstream ss;
//		ss <<  object + "\\descriptors\\" + imageNumber + ".png";
//		string s = ss.str();
//		cv::imwrite(s, descriptors_ref);
//		ContourMatchingDescriptor ref_descriptor = ContourMatchingDescriptor::buildFromKeyPoints(getMaxContour(im_ref, "name"), im_ref.cols, im_ref.rows, keypoints_ref);
//		ref_descriptor.writeDescriptor(object + "/refOut/" + imageNumber + ".json");
//
//		/*cv::BFMatcher bf = cv::BFMatcher(cv::NORM_HAMMING2);
//		std::vector<cv::DMatch> matches;
//		bf.match(descriptors_ref, descriptors, matches);
//		std::vector< DMatch > good_matches;
//
//		double max_dist = 0; double min_dist = 100;
//		for (int i = 0; i < matches.size(); i++)
//		{
//			double dist = matches[i].distance;
//			if (dist < min_dist) min_dist = dist;
//			if (dist > max_dist) max_dist = dist;
//		}
//
//		for (int i = 0; i < matches.size(); i++)
//		{
//			if (matches[i].distance < max(2 * min_dist, 0.03))
//			{
//				good_matches.push_back(matches[i]);
//			}
//		}
//
//		vector<Point> matchedRefPoints;
//		vector<Point> matchedPoints;
//		for (DMatch match : good_matches) {
//			matchedPoints.push_back(descriptor.getMatchingPoints()[match.trainIdx]);
//			matchedRefPoints.push_back(ref_descriptor.getMatchingPoints()[match.queryIdx]);
//		}
//		
//
//		Mat tranformMatrix = findHomography(matchedRefPoints, matchedPoints, CV_RANSAC);
//		cv::Matx33d warp = tranformMatrix;
//		vector<Point> contour = ref_descriptor.getContour();
//		vector<Point3d> trasContour;
//		for (Point p : contour) {
//			trasContour.push_back(warp * Point2d(p));
//		}
//
//		for (int i = 0; i < trasContour.size() - 1; i++) {
//			Point p1(trasContour.at(i).x, trasContour.at(i).y);
//			Point p2(trasContour.at(i + 1).x, trasContour.at(i + 1).y);
//			
//			line(im, p1, p2, Scalar(255, 0, 0), 2);
//		}
//
//		Mat img_matches;
//		cv::drawMatches(im, keypoints, im_ref, keypoints_ref,
//			good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
//			vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);*/
//		//-- Show detected matches
//		//cv::resize(img_matches, img_matches, Size(888, 888));
//		//cv::imshow("Good Matches", img_matches);
//
//		//cv::resize(im, im, Size(888, 888));
//		//cv::imshow(s, im);
//
//
//		//cv::resize(im_ref, im_ref, Size(888, 888));
//		//cv::imshow("ref", im_ref);
//		//break;
//	}
//	//waitKey(0);
//
//}
//
//std::vector<cv::Point> getMaxContour(Mat img, string name) {
//	Mat canny_output;
//	vector<vector<Point> > contours;
//	vector<Vec4i> hierarchy;
//
//	int thresh = 128;
//	int max_thresh = 255;
//	/// Detect edges using canny
//	Canny(img, canny_output, thresh, thresh * 2, 3);
//
//	int dilation_size = 1;
//	Mat element = getStructuringElement(MORPH_RECT,
//		Size(2 * dilation_size + 1, 2 * dilation_size + 1),
//		Point(dilation_size, dilation_size));
//	/// Apply the dilation operation
//	Mat dilation_dst;
//	dilate(canny_output, dilation_dst, element);
//
//	/// Find contours
//	findContours(dilation_dst, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
//
//	//find lagrest contour
//	Mat drawing = Mat::zeros(dilation_dst.size(), CV_8UC3);
//	double maxConturSize = 0;
//	int index = 0;
//	for (int i = 0; i< contours.size(); i++)
//	{
//		double size = contourArea(contours[i]);
//		if (maxConturSize < size) {
//			index = i;
//			maxConturSize = size;
//		}
//	}
//	Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
//	drawContours(drawing, contours, index, color, 2, 8, hierarchy, 0, Point());
//
//	return contours.at(index);
//}