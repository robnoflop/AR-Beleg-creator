#pragma once

#include <opencv2\imgproc.hpp>

class ActionPointPosition
{
private:
	cv::Point2d ancor;
	bool acive = false;
	int informationId;

public:
	ActionPointPosition(cv::Point2d ancor, int informationId);
	~ActionPointPosition();
	cv::Point2d getAncor();
	void setAncor(cv::Point2d newAncor);
};

