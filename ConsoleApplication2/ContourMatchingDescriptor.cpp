#include "ContourMatchingDescriptor.h"
#include <math.h>
#include <iostream>
#include <fstream>
#include <stdio.h>

ContourMatchingDescriptor ContourMatchingDescriptor::buildFromContour(std::vector<cv::Point> contour, int cols, int rows) {
	std::vector<cv::Point2d> matchingPoints;

	//find heeghst contour point
	double minY = 1000000;
	int minYindex = 0;
	for (int i = 0; i < contour.size(); i++) {
		if (contour[i].y < minY) {
			minYindex = i;
			minY = contour[i].y;
		}
	}

	cv::Point maxPoint = contour[minYindex];
	matchingPoints.push_back(maxPoint);

	/// Get the moments
	cv::Moments mu = moments(contour, false);
	cv::Point2d massCenter(mu.m10 / mu.m00, mu.m01 / mu.m00);
	matchingPoints.push_back(massCenter);

	cv::Rect boundRect = boundingRect(contour);
	matchingPoints.push_back(cv::Point2d(boundRect.x, boundRect.y));
	matchingPoints.push_back(cv::Point2d(boundRect.x + boundRect.width, boundRect.y));
	matchingPoints.push_back(cv::Point2d(boundRect.x, boundRect.y + boundRect.height));
	matchingPoints.push_back(cv::Point2d(boundRect.x + boundRect.width, boundRect.y + boundRect.height));

	cv::RotatedRect rec = fitEllipse(contour);
	boundRect = rec.boundingRect();
	matchingPoints.push_back(rec.center);
	matchingPoints.push_back(cv::Point2d(boundRect.x, boundRect.y));
	matchingPoints.push_back(cv::Point2d(boundRect.x + boundRect.width, boundRect.y));
	matchingPoints.push_back(cv::Point2d(boundRect.x, boundRect.y + boundRect.height));
	matchingPoints.push_back(cv::Point2d(boundRect.x + boundRect.width, boundRect.y + boundRect.height));

	rec = minAreaRect(contour);
	boundRect = rec.boundingRect();
	matchingPoints.push_back(rec.center);
	matchingPoints.push_back(cv::Point2d(boundRect.x, boundRect.y));
	matchingPoints.push_back(cv::Point2d(boundRect.x + boundRect.width, boundRect.y));
	matchingPoints.push_back(cv::Point2d(boundRect.x, boundRect.y + boundRect.height));
	matchingPoints.push_back(cv::Point2d(boundRect.x + boundRect.width, boundRect.y + boundRect.height));

	cv::Point2f center;
	float r;
	minEnclosingCircle(contour, center, r);
	matchingPoints.push_back(center);

	return ContourMatchingDescriptor(matchingPoints, contour, cols, rows);
}

ContourMatchingDescriptor ContourMatchingDescriptor::buildFromKeyPoints(std::vector<cv::Point> contour, int cols, int rows, std::vector<cv::KeyPoint> keyPoints) {
	std::vector<cv::Point2d> matchingPoints;
	for (cv::KeyPoint point : keyPoints) {
		matchingPoints.push_back(point.pt);
	}
	return ContourMatchingDescriptor(matchingPoints, contour, cols, rows);
}


ContourMatchingDescriptor::ContourMatchingDescriptor(std::vector<cv::Point2d> matchingPoints,
	std::vector<cv::Point> contour,
	int xScale,
	int yScale)
{
	this->matchingPoints = matchingPoints;
	this->contour = contour;
	this->xScale = xScale;
	this->yScale = yScale;
	createMatchingPointDescription();
}


ContourMatchingDescriptor::~ContourMatchingDescriptor()
{

}

std::vector<cv::Point2d> ContourMatchingDescriptor::getMatchingPoints() {
	return matchingPoints;
}

std::vector<cv::Point2d> ContourMatchingDescriptor::getMatchingPointsDescriptions() {
	return matchingPointsDescriptions;
}

std::vector<cv::Point> ContourMatchingDescriptor::getContour() {
	return contour;
}

cv::Point2d ContourMatchingDescriptor::scalePointDown(cv::Point2d p) {
	return cv::Point2d(p.x / xScale, p.y / yScale);
}

cv::Point ContourMatchingDescriptor::scalePointUp(cv::Point2d p) {
	return cv::Point(p.x * xScale, p.y * yScale);
}

void ContourMatchingDescriptor::createMatchingPointDescription() {
	for (cv::Point2d p : matchingPoints) {
		matchingPointsDescriptions.push_back(scalePointDown(p));
	}
}


int ContourMatchingDescriptor::getDistance(cv::Point p1, cv::Point p2) {
	return  abs((p1.x - p2.x) + (p1.y - p2.y));
}

std::vector<cv::Point> ContourMatchingDescriptor::translateReferencePointsForCenter(ContourMatchingDescriptor refDescriptor) {
	std::vector<cv::Point> translatedRefPoints;

	for (cv::Point2d p : refDescriptor.getMatchingPointsDescriptions()) {
		translatedRefPoints.push_back(scalePointUp(p));
	}
	return translatedRefPoints;
}

void ContourMatchingDescriptor::writeDescriptor(std::string fileName) {
	std::ofstream content;
	content.open(fileName);
	content << "{";
	content << "\"matchingPoints\" : [";
	for (int i = 0; i < matchingPoints.size(); i++) {
		content << "{\"point\":[" << matchingPoints.at(i).x << "," << matchingPoints.at(i).y << "]}";
		if (i < matchingPoints.size() - 1) {
			content << ",\n";
		} else {
			content << "\n";
		}
	}
	content << "],\n";

	content << "\"matchingPointsDescriptions\" : [";
	for (int i = 0; i < matchingPointsDescriptions.size(); i++) {
		content << "{\"point\":[" << matchingPointsDescriptions.at(i).x << "," << matchingPointsDescriptions.at(i).y << "]}";
		if (i < matchingPointsDescriptions.size() - 1) {
			content << ",\n";
		}
		else {
			content << "\n";
		}
	}
	content << "],\n";
	content << "\"contour\": [";
	for (int i = 0; i < contour.size(); i++) {
		content << "{\"point\":[" << contour.at(i).x << "," << contour.at(i).y << "]}";
		if (i < contour.size() - 1) {
			content << ",\n";
		}
		else {
			content << "\n";
		}
	}
	content << "],\n";
	content << "\"xScale\" : " << xScale << ",";
	content << "\"yScale\" : " << yScale;
	content << "}";
	content.close();
}
