//#include <stdio.h>
//#include <iostream>
//#include <stdio.h>
//#include <iostream>
//#include <sstream>
//#include "opencv2/core.hpp"
//#include "opencv2/features2d.hpp"
//#include "opencv2/imgcodecs.hpp"
//#include "opencv2/highgui.hpp"
//#include <opencv2/opencv.hpp>
//
//using namespace cv;
//using namespace std;
//
//RNG rng(12345);
//
//std::vector<cv::Point> getMaxContour(Mat img) {
//	Mat canny_output;
//	vector<vector<Point> > contours;
//	vector<Vec4i> hierarchy;
//
//	int thresh = 128;
//	int max_thresh = 255;
//	/// Detect edges using canny
//	Canny(img, canny_output, thresh, thresh * 2, 3);
//
//	int dilation_size = 1;
//	Mat element = getStructuringElement(MORPH_RECT,
//		Size(2 * dilation_size + 1, 2 * dilation_size + 1),
//		Point(dilation_size, dilation_size));
//	/// Apply the dilation operation
//	Mat dilation_dst;
//	dilate(canny_output, dilation_dst, element);
//
//	/// Find contours
//	findContours(dilation_dst, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
//
//	//find lagrest contour
//	Mat drawing = Mat::zeros(dilation_dst.size(), CV_8UC3);
//	double maxConturSize = 0;
//	int index = 0;
//	for (int i = 0; i< contours.size(); i++)
//	{
//		double size = contourArea(contours[i]);
//		if (maxConturSize < size) {
//			index = i;
//			maxConturSize = size;
//		}
//	}
//	Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
//	drawContours(drawing, contours, index, color, 2, 8, hierarchy, 0, Point());
//
//	return contours.at(index);
//}
//
//int main(int argc, char *argv[])
//{
//	Mat img_object = imread("_arena/ref/04.jpg", CV_LOAD_IMAGE_GRAYSCALE);
//	Mat img_scene = imread("_arena/04.jpg", CV_LOAD_IMAGE_GRAYSCALE);
//
//	Point2f src_center(img_scene.cols / 2.0F, img_scene.rows / 2.0F);
//	Mat rot_mat = getRotationMatrix2D(src_center, 90, 1.0);
//	Mat dst;
//	warpAffine(img_scene, dst, rot_mat, img_scene.size());
//	img_scene = dst;
//
//	//-- Step 1: Detect the keypoints using SURF Detector
//	Ptr<ORB> detector = ORB::create(4000);
//
//	std::vector<KeyPoint> keypoints_object, keypoints_scene;
//	Mat descriptors_object, descriptors_scene;
//	detector->detectAndCompute(img_object, Mat(), keypoints_object, descriptors_object);
//	Ptr<ORB> detector2 = ORB::create();
//	detector2->detectAndCompute(img_scene, Mat(), keypoints_scene, descriptors_scene);
//
//	//-- Step 3: Matching descriptor vectors using FLANN matcher
//	cv::BFMatcher bf = cv::BFMatcher(cv::NORM_HAMMING2);
//	std::vector< DMatch > matches;
//	std::vector< DMatch > matchesCross;
//	bf.match(descriptors_object, descriptors_scene, matches);
//	bf.match(descriptors_scene, descriptors_object, matchesCross);
//
//	//double max_dist = 0; double min_dist = 100;
//
//	////-- Quick calculation of max and min distances between keypoints
//	//for (int i = 0; i < descriptors_scene.rows; i++)
//	//{
//	//	double dist = matches[i].distance;
//	//	if (dist < min_dist) min_dist = dist;
//	//	if (dist > max_dist) max_dist = dist;
//	//}
//
//	/*printf("-- Max dist : %f \n", max_dist);
//	printf("-- Min dist : %f \n", min_dist);
//*/
//	std::vector< DMatch > good_matches;
//	for (int i = 0; i < matches.size(); i++) {
//		DMatch forward = matches.at(i);
//		DMatch backward = matchesCross.at(forward.trainIdx);
//		if (backward.trainIdx == forward.queryIdx) {
//			good_matches.push_back(forward);
//		}
//	}
//
//
//	//-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
//	
//	/*for (int i = 0; i < descriptors_object.rows; i++)
//	{
//		if (matches[i].distance < max(2 * min_dist, 0.03))
//		{
//			good_matches.push_back(matches[i]);
//		}
//	}*/
//
//	Mat img_matches;
//	drawMatches(img_object, keypoints_object, img_scene, keypoints_scene,
//		good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
//		std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
//
//	//-- Localize the object
//	std::vector<Point2f> obj;
//	std::vector<Point2f> scene;
//
//	for (int i = 0; i < good_matches.size(); i++)
//	{
//		//-- Get the keypoints from the good matches
//		obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
//		scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
//	}
//
//	Mat H = findHomography(obj, scene, CV_RANSAC);
//
//	//-- Get the corners from the image_1 ( the object to be "detected" )
//	std::vector<Point2f> obj_corners(4);
//	obj_corners[0] = cvPoint(0, 0); obj_corners[1] = cvPoint(img_object.cols, 0);
//	obj_corners[2] = cvPoint(img_object.cols, img_object.rows); obj_corners[3] = cvPoint(0, img_object.rows);
//	std::vector<Point2f> scene_corners(4);
//
//	perspectiveTransform(obj_corners, scene_corners, H);
//
//	//-- Draw lines between the corners (the mapped object in the scene - image_2 )
//	line(img_matches, scene_corners[0] + Point2f(img_object.cols, 0), scene_corners[1] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
//	line(img_matches, scene_corners[1] + Point2f(img_object.cols, 0), scene_corners[2] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
//	line(img_matches, scene_corners[2] + Point2f(img_object.cols, 0), scene_corners[3] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
//	line(img_matches, scene_corners[3] + Point2f(img_object.cols, 0), scene_corners[0] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
//
//
//	std::vector<cv::Point> contour = getMaxContour(img_object);
//
//	Mat tranformMatrix = findHomography(scene, scene, CV_RANSAC);
//	cout << "M = " << endl << " " << tranformMatrix << endl << endl;
//
//	tranformMatrix = findHomography(obj, obj, CV_RANSAC);
//	cout << "M = " << endl << " " << tranformMatrix << endl << endl;
//
//	tranformMatrix = findHomography(obj, scene, CV_RANSAC);
//	cout << "M = " << endl << " " << tranformMatrix << endl << endl;
//
//	Rect objRect = boundingRect(obj);
//	Rect sceneRect = boundingRect(scene);
//
//	std::vector<Point2f> objRectPoints;
//	objRectPoints.push_back(Point2f(objRect.x, objRect.y));
//	objRectPoints.push_back(Point2f(objRect.x + objRect.width, objRect.y));
//	objRectPoints.push_back(Point2f(objRect.x, objRect.y + objRect.height));
//	objRectPoints.push_back(Point2f(objRect.x + objRect.width, objRect.y + objRect.height));
//	std::vector<Point2f> sceneRectPoints;
//	sceneRectPoints.push_back(Point2f(sceneRect.x, sceneRect.y));
//	sceneRectPoints.push_back(Point2f(sceneRect.x + sceneRect.width, sceneRect.y));
//	sceneRectPoints.push_back(Point2f(sceneRect.x, sceneRect.y + sceneRect.height));
//	sceneRectPoints.push_back(Point2f(sceneRect.x + sceneRect.width, sceneRect.y + sceneRect.height));
//
//	//tranformMatrix = findHomography(objRectPoints, sceneRectPoints, CV_RANSAC);
//	cout << "M = " << endl << " " << tranformMatrix << endl << endl;
//
//	cv::Matx33d warp = tranformMatrix;
//	vector<Point3d> trasContour;
//	for (Point p : contour) {
//		trasContour.push_back(warp * Point2d(p));
//	}
//
//	for (int i = 0; i < trasContour.size() - 1; i++) {
//		Point p1(trasContour.at(i).x, trasContour.at(i).y);
//		Point p2(trasContour.at(i + 1).x, trasContour.at(i + 1).y);
//
//		line(img_scene, p1, p2, Scalar(255, 0, 0), 2);
//	}
//
//	//-- Show detected matches
//	cv::resize(img_scene, img_scene, Size(888, 888));
//	imshow("contour", img_scene);
//
//
//	cv::resize(img_matches, img_matches, Size(888, 888));
//	imshow("Good Matches & Object detection", img_matches);
//
//	waitKey(0);
//	return 0;
//}