//#include "opencv2/imgproc.hpp"
//#include <opencv2/highgui.hpp>
//#include <opencv2\calib3d.hpp>
//#include "ContourMatchingDescriptor.h"
//#include "ActionPointPosition.h"
//#include <iostream>
//#include <stdlib.h>
//#include <vector>
//
//using namespace cv;
//using namespace std;
//
//Mat refImage;
//Mat testImage;
//int thresh = 128;
//int max_thresh = 255;
//RNG rng(12345);
//
///// Function header
//ContourMatchingDescriptor createObjectDescription(Mat img, string name);
//
//int main(int argc, const char* argv[]) {
//	std::string names[] = { "01",
//		"02",
//		"03",
//		"04",
//		"05",
//		"06",
//		"07",
//		"08",
//		"09",
//		"10",
//		"11",
//		"12",
//		"13",
//		"14",
//		"15",
//		"16",
//		"17",
//		"18",
//		"19",
//		"20",
//		"21",
//		"22",
//		"23",
//		"24",
//		"25",
//		"26",
//		"27",
//		"28",
//		"29",
//		"30",
//		"31",
//		"32",
//		"33",
//		"34",
//		"35",
//		"36",
//		"37",
//		"38",
//		"39",
//		"40",
//		"41",
//		"42",
//		"43",
//		"44",
//		"45",
//		"46",
//		"47" };
//		
//	for (std::string imageNumber : names)
//	{
//		std::string object = "_tor";
//
//		refImage = cv::imread(object + "/ref/" + imageNumber + ".jpg", 1);
//		ContourMatchingDescriptor refDescriptor = createObjectDescription(refImage, "refImage");
//		vector<ActionPointPosition> actionPoints;
//		for (int i = 0; i < 10; i++) {
//			int x = i * 10; //rng.uniform(0, refImage.cols);
//			int y = i * 10; //rng.uniform(0, refImage.rows);
//			Point2d ancor = refDescriptor.scalePointDown(Point2d(x, y));
//			actionPoints.push_back(ActionPointPosition(ancor, i));
//		}
//
//		refDescriptor.writeDescriptor(object + "/refOut/" + imageNumber + ".json");
//
//		/*testImage = cv::imread(object + "/" + imageNumber + ".jpg", 1);
//		ContourMatchingDescriptor testDescriptor = createObjectDescription(testImage, "testImage");
//
//		vector<Point> translatedRefPoints = testDescriptor.translateReferencePointsForCenter(refDescriptor);
//		Mat tranformMatrix = findHomography(refDescriptor.getMatchingPoints(), translatedRefPoints);
//		cv::Matx33d warp = tranformMatrix;
//
//		vector<Point> contour = refDescriptor.getContour();
//		vector<Point3d> trasContour;
//		for (Point p : contour) {
//			trasContour.push_back(warp * Point2d(p));
//		}
//
//		for (int i = 0; i < actionPoints.size(); i++) {
//			ActionPointPosition p = actionPoints.at(i);
//			Point2d ancor = testDescriptor.scalePointUp(p.getAncor());
//			Point3d warpedAncor = warp * ancor;
//			p.setAncor(Point2d(warpedAncor.x, warpedAncor.y));
//			actionPoints[i] = p;
//		}
//
//		for (int i = 0; i < trasContour.size() - 1; i++) {
//			Point p1(trasContour.at(i).x, trasContour.at(i).y);
//			Point p2(trasContour.at(i + 1).x, trasContour.at(i + 1).y);
//
//			line(testImage, p1, p2, Scalar(255, 0, 0), 2);
//		}
//
//		for (ActionPointPosition p : actionPoints) {
//			circle(testImage, p.getAncor(), 10, Scalar(0, 0, 255), 2);
//		}
//
//		/// Show in a window
//		namedWindow("translatedPoints", CV_WINDOW_AUTOSIZE);
//		imshow("translatedPoints", testImage);*/
//
//	}
//}
//
//ContourMatchingDescriptor createObjectDescription(Mat img, string name) {
//	
//	vector<Point> refContour;
//
//
//	Mat canny_output;
//	vector<vector<Point> > contours;
//	vector<Vec4i> hierarchy;
//
//	/// Detect edges using canny
//	Canny(img, canny_output, thresh, thresh * 2, 3);
//
//	int dilation_size = 1;
//	Mat element = getStructuringElement(MORPH_RECT,
//		Size(2 * dilation_size + 1, 2 * dilation_size + 1),
//		Point(dilation_size, dilation_size));
//	/// Apply the dilation operation
//	Mat dilation_dst;
//	dilate(canny_output, dilation_dst, element);
//
//	/// Find contours
//	findContours(dilation_dst, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
//
//	//find lagrest contour
//	Mat drawing = Mat::zeros(dilation_dst.size(), CV_8UC3);
//	double maxConturSize = 0;
//	int index = 0;
//	for (int i = 0; i< contours.size(); i++)
//	{
//		double size = contourArea(contours[i]);
//		if (maxConturSize < size) {
//			index = i;
//			maxConturSize = size;
//		}
//	}
//	Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
//	drawContours(drawing, contours, index, color, 2, 8, hierarchy, 0, Point());
//
//	refContour = contours.at(index);
//	
//	return ContourMatchingDescriptor::buildFromContour(refContour, img.cols, img.rows);
//}