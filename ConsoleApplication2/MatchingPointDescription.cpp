#include "MatchingPointDescription.h"



MatchingPointDescription::MatchingPointDescription(float angle, float distance)
{
	this->angle = angle;
	this->distance = distance;
}


MatchingPointDescription::~MatchingPointDescription()
{
}

float MatchingPointDescription::getAngle() {
	return angle;
}

float MatchingPointDescription::getDistance() {
	return distance;
}