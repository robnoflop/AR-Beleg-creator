#pragma once
#include <vector>
#include <opencv2/imgproc.hpp>

class ContourMatchingDescriptor
{

private:
	std::vector<cv::Point2d> matchingPoints;
	std::vector<cv::Point2d> matchingPointsDescriptions;
	std::vector<cv::Point> contour;
	cv::Point massCenter;
	cv::Point top;
	int xScale;
	int yScale;

	void createMatchingPointDescription();
	int getDistance(cv::Point p1, cv::Point p2);

public:
	static ContourMatchingDescriptor buildFromContour(std::vector<cv::Point> contour, int cols, int rows);
	static ContourMatchingDescriptor buildFromKeyPoints(std::vector<cv::Point> contour, int cols, int rows, std::vector<cv::KeyPoint> keyPoints);

	ContourMatchingDescriptor(std::vector<cv::Point2d> matchingPoints, std::vector<cv::Point> refContour, int xScale, int yScale);
	~ContourMatchingDescriptor();
	std::vector<cv::Point2d> getMatchingPoints();
	std::vector<cv::Point2d> getMatchingPointsDescriptions();
	std::vector<cv::Point> getContour();
	cv::Point2d scalePointDown(cv::Point2d p);
	cv::Point scalePointUp(cv::Point2d p);

	void writeDescriptor(std::string fileName);

	std::vector<cv::Point> translateReferencePointsForCenter(ContourMatchingDescriptor refDescriptor);
};

